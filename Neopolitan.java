import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Neopolitan extends  Pizza {

    public Neopolitan(){
        super("Neopolitan",15,1, new ArrayList<Ingredient>(
                Arrays.asList(new Ingredient [] {new Mozarella(),new Tomatoes(),new PizzaSauce() }
                ))
        );
    }
    public String toString(){
        return "Neapolitan Pizza";
    }
}
