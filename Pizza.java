import java.util.*;
public class Pizza extends Restaurant {
    private  List<Ingredient> ingredients;
    public List<Ingredient> getIngredients() {
        return ingredients;
    }
    public Pizza(String name, Integer price , Integer ID, List<Ingredient> ingredients){
        super (name,price,ID);
        this.ingredients=ingredients;
    }
    public Pizza (){
        super ();
        ingredients=new ArrayList<>();
    }
    public void removeIngredient(Integer id){
        if(id==0) return;
        int removeIndex=-1;
        for(int i=0;i<ingredients.size();i++){
            if(ingredients.get(i).getId()== id){
                removeIndex=i;
                break;
            }
        }
        if(removeIndex==-1){
            System.out.println("Ingredient not found.");
        }
        else{
            int price =ingredients.get(removeIndex).getPrice();
            Ingredient remove =ingredients.get(removeIndex);
            ingredients.remove(remove);
            this.setPrice(this.getPrice()-price);
            System.out.println(String.format("Ingredient id %s has been removed .New price is $%s",id,this.getPrice()));
        }
    }
}
