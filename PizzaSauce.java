public class PizzaSauce extends Ingredient{
    public PizzaSauce(){
        super("Pizza Sauce",1,6);
    }
    public String toString (){
        return "Pizza Sauce";
    }
}
